# frozen_string_literal: true

module DeviseLdapAuthenticatable
  VERSION = '0.8.6'
end
