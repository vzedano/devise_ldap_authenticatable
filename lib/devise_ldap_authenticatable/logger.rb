# frozen_string_literal: true

module DeviseLdapAuthenticatable
  class Logger
    def self.send(message, logger = Rails.logger)
      logger.add 0, "  \e[36mLDAP:\e[0m #{message}" if ::Devise.ldap_logger
    end
  end
end
