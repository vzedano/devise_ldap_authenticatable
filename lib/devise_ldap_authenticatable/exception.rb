# frozen_string_literal: true

module DeviseLdapAuthenticatable
  class LdapException < RuntimeError
  end
end
